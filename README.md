# Google App Engine Standard Spring Archetype

[Maven Archetype](https://maven.apache.org/archetype/maven-archetype-plugin/plugin-info.html) to create a project:

* with Spring Boot ...
* deployed to Google App Engine ...
* using Bitbucket pipelines

Inspired on [Deploy Spring Boot Application in App Engine standard](https://codelabs.developers.google.com/codelabs/cloud-app-engine-springboot).

## Create a project using this archetype

Execute this goal:

```
mvn archetype:generate \
  -DarchetypeGroupId=com.rogervinas \
  -DarchetypeArtifactId=gae-standard-spring-archetype \
  -DarchetypeVersion=1.0.0-SNAPSHOT
```

And fill prompted values:

* groupId / artifactId / version : Generated Maven artifact coordinates.
* name : Name of the generated project.
* gae.application : Name of the Google App Engine application to deploy to.

First time you may have to execute `mvn archetype:crawl` to update your local catalog of archetypes.

## Integration Test

This archetype has one [sample project](src/test/resources/projects/sample) to be executed by [archetype:integration-test](https://maven.apache.org/archetype/maven-archetype-plugin/integration-test-mojo.html) that will:

* Generate a sample project from the archetype.
* Execute `mvn test` goal on the generated sample project.

It is not the case in this archetype but as stated in [archetype:integration-test](https://maven.apache.org/archetype/maven-archetype-plugin/integration-test-mojo.html) it would be possible also to add an extra check comparing the files generated with the ones in `projects/sample/reference` directory.