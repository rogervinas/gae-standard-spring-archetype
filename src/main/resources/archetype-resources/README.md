#set( $h1 = '#' )
#set( $h2 = '##' )
$h1 ${name}

Created from [Google App Engine Standard Spring Archetype](https://bitbucket.org/rogervinas/gae-standard-spring-archetype).

Please document the following:

* Why this service was created?
* What this service does?
* How this service does what it does?

$h2 Build & Test

```
mvn clean install
```

$h2 Run locally

```
mvn appengine:devserver
```

$h2 Test locally

Using [HTTPie](https://httpie.org/):

```
http http://localhost:8080/hello name==John
```

Using plain curl:

```
curl -w '\n' http://localhost:8080/hello?name=John
```

$h2 Deploy to Google App Engine

Branch master is continuously deployed to Google App Engine using Bitbucket pipelines.

You must create a Service Account Key for your Google App:

* Go to Google Cloud Platform console > APIs & Services:
    * Enable App Engine Admin API
* Go to Google Cloud Platform console > IAM & admin > Service accounts:
    * Create Service Account, name it **bitbucket** with role Editor.
    * Create a Key and download in JSON format.
* Go to Bitbucket > Settings > Pipelines > Environment variables:
    * Create GOOGLE_SERVICE_ACCOUNT_KEY variable and paste the contents of the JSON file downloaded before.
    * Select "Mask and encrypt the variable".

$h2 Test on Google App Engine

Using [HTTPie](https://httpie.org/):

```
http https://${gaeApplication}.appspot.com/hello name==John
```

Using plain curl:

```
curl -w '\n' https://${gaeApplication}.appspot.com/hello?name=John
```
